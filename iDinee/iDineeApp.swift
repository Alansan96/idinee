//
//  iDineeApp.swift
//  iDinee
//
//  Created by Alan Santoso on 13/10/20.
//

import SwiftUI

@main
struct iDineeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
